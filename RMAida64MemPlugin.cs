﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Linq;

using Rainmeter;

namespace RMAida64MemPlugin
{
    public class RMAida64MemPlugin
    {
        private API _ThisApi                    = null;
        private Aida64ShMemManager _A64Manager  = null;
        private SharedMemXml _LastDataRead      = null;

        private Aida64MeterType _CurrentType    = Aida64MeterType.Undefined;
        private String _CurrentA64MeterId       = String.Empty;
        private String _CurrentA64StrValue      = String.Empty;

        /// <summary>
        /// Constructeur de la classe RMAida64MemPlugin
        /// Lié à la méthode Initialize de l'API Rainmeter
        /// Faire les initialisations uniques ici (Singleton, etc...)
        /// --
        /// Called when a measure is created (i.e. when a skin is loaded or when a skin is refreshed). Create your measure object here. Any other initialization or code that only needs to happen once should be placed here.
        /// </summary>
        public RMAida64MemPlugin()
        {
            // ENABLE DEBUG
            // System.Diagnostics.Debugger.Launch();
            // ENABLE DEBUG

            this._A64Manager = new Aida64ShMemManager();
            this._A64Manager.OpenView();

            this._LastDataRead = this._A64Manager.GetData();

        }

        /// <summary>
        /// Called by Rainmeter when the measure settings are to be read directly after Initialize. 
        /// If DynamicVariables=1 is set on the measure, this function is called just before every call to the Update function during the update cycle.
        /// --
        /// Fonction appelée après l'initialisation d'un meter ou juste avant l'appel à la fonction Update avant un cycle d'update
        /// </summary>
        /// <param name="Api">Référence à l'API Rainmeter</param>
        /// <param name="MaxValue">Valeur maximale (utilisée entre autres pour déterminer les pourcentages)</param>
        public void Reload(API Api, ref double MaxValue)
        {
            String A64MeterTypeInput            = String.Empty;

            this._ThisApi                       = Api;

            if (this._LastDataRead == null)
            {
                this._ThisApi.Log(API.LogType.Error, "-- Aida 64 : No Data read from shared memory / Aida 64 : Aucune valeur lue en mémoire partagée --");
            }
            else
            {
                this._ThisApi.Log(API.LogType.Debug, "-- Aida 64 : Data read from shared memory / Aida 64 : Valeurs lues en mémoire partagée --");

                foreach (Meter ThisMeter in this._LastDataRead.Pwr) { this._ThisApi.Log(API.LogType.Debug, $"[Pwr Meter] Id: {ThisMeter.Id} Label: {ThisMeter.Label} Value: {ThisMeter.Value}"); }
                foreach (Meter ThisMeter in this._LastDataRead.Sys) { this._ThisApi.Log(API.LogType.Debug, $"[Sys Meter] Id: {ThisMeter.Id} Label: {ThisMeter.Label} Value: {ThisMeter.Value}"); }
                foreach (Meter ThisMeter in this._LastDataRead.Temp) { this._ThisApi.Log(API.LogType.Debug, $"[Temp Meter] Id: {ThisMeter.Id} Label: {ThisMeter.Label} Value: {ThisMeter.Value}"); }
                foreach (Meter ThisMeter in this._LastDataRead.Volt) { this._ThisApi.Log(API.LogType.Debug, $"[Volt Meter] Id: {ThisMeter.Id} Label: {ThisMeter.Label} Value: {ThisMeter.Value}"); }
            }

            /* 
            Lors de l'actualisation, on va pouvoir ici lire les propriétés du meter configuré
            Exemple : string type = api.ReadString("Type", "");
            
            [rma64memplugin]
            Measure=Plugin
            Plugin=RMAida64MemPlugin.dll
            Type=String

            */

            A64MeterTypeInput                   = this._ThisApi.ReadString("A64MeterType", "");
            this._CurrentA64MeterId             = this._ThisApi.ReadString("A64MeterId", "");

            if (String.IsNullOrEmpty(A64MeterTypeInput))
            {
                this._ThisApi.Log(API.LogType.Error, "-- Missing Rainmeter property : A64MeterType / Propriété Rainmeter manquante : A64MeterType --");
            } else
            {
                try
                {
                    this._CurrentType           = (Aida64MeterType)Enum.Parse(typeof(Aida64MeterType), A64MeterTypeInput, true);
                } catch (Exception) {
                    this._CurrentType = Aida64MeterType.Undefined;
                    this._ThisApi.Log(API.LogType.Error, "-- A64MeterTypeInput value is not valid or not yet supported by this Plugin / La valeur de la propriété A64MeterTypeInput est invalide ou n'est pas encore gérée par ce plugin --");
                }

            }

            if (String.IsNullOrEmpty(this._CurrentA64MeterId))
            {
                this._ThisApi.Log(API.LogType.Error, "-- Missing Rainmeter property : A64MeterId / Propriété Rainmeter manquante : A64MeterId --");
            }

        }

        /// <summary>
        /// Called by Rainmeter when a measure value is to be updated (i.e. on each update cycle). The number returned represents the number value of the measure.
        /// --
        /// Update / Utile pour le retour des valeur sous la forme nombre décimal. Il faut également mettre à jour les variables locales déjà préparées ici au lieu de GetString()
        /// </summary>
        /// <returns>Valeur sous la forme nombre décimal</returns>
        public Double Update()
        {
            String CurrentStrValue                  = String.Empty;
            Meter CurrentMeter                      = null;
            Double CurrentValue                     = 0.0;

            // Si l'on détecte un cas de nombre de type nombre décimal
            try
            {
                this._LastDataRead = this._A64Manager.GetData();
            } catch (Exception Ex) {
                this._LastDataRead = null;
                this._ThisApi.Log(API.LogType.Error, $"-- Exception while reading Aida 64 Shared Memory Values : {Ex.Message + (Ex.InnerException != null ? "\r\n" + Ex.InnerException.ToString() : "")}--");

            }
            this._CurrentA64StrValue                = String.Empty;

            if (this._LastDataRead == null)
            {
                this._ThisApi.Log(API.LogType.Error, "-- No Data Read from Aida 64 : Is Aida 64 running ? / Aucuné donnée lue de Aida 64 : Aida 64 est-il en cours d'exécution --");
            } else if (this._CurrentType != Aida64MeterType.Undefined)
            {
                switch (this._CurrentType)
                {
                    case Aida64MeterType.Pwr:
                        CurrentMeter                = this._LastDataRead.Pwr.FirstOrDefault(d => String.Compare(d.Id, this._CurrentA64MeterId, StringComparison.OrdinalIgnoreCase) == 0);
                        break;

                    case Aida64MeterType.Sys:
                        CurrentMeter                = this._LastDataRead.Sys.FirstOrDefault(d => String.Compare(d.Id, this._CurrentA64MeterId, StringComparison.OrdinalIgnoreCase) == 0);
                        break;

                    case Aida64MeterType.Temp:
                        CurrentMeter                = this._LastDataRead.Temp.FirstOrDefault(d => String.Compare(d.Id, this._CurrentA64MeterId, StringComparison.OrdinalIgnoreCase) == 0);
                        break;

                    case Aida64MeterType.Volt:
                        CurrentMeter                = this._LastDataRead.Volt.FirstOrDefault(d => String.Compare(d.Id, this._CurrentA64MeterId, StringComparison.OrdinalIgnoreCase) == 0);
                        break;

                    default: break;
                }

                if (CurrentMeter == null)
                {
                    this._ThisApi.Log(API.LogType.Error, $"-- Can't find Aida 64 data with Id: {this._CurrentA64MeterId} / Impossible de trouver la donnée Aida 64 pour l'Id: {this._CurrentA64MeterId}");
                } else
                {
                    CurrentStrValue = CurrentMeter.Value;

                    if (Double.TryParse(CurrentStrValue, out CurrentValue))
                    {
                        return CurrentValue;
                    } else
                    {
                        this._CurrentA64StrValue    = CurrentStrValue;
                    }
                }
            }


            // Sinon return 0.0
            return CurrentValue;
        }

        /// <summary>
        /// Optional function that returns the string value of the measure. 
        /// Since this function is called 'on-demand' and may be called multiple times during the update cycle, do not process any data or consume CPU in this function.
        /// Do as minimal processing as possible to return the desired string.
        /// It is recommended to do all processing during the Update function and set a string variable there and retrieve that string variable in this function.
        /// The return value must be marshalled from a C# style string to a C style string (WCHAR*).
        /// --
        /// Utilisé si la mesure est de type chaîne de caractères (ou autre castable en chaîne de caractères)
        /// </summary>
        /// <returns>Valeur sous la forme chaîne de caractères</returns>
        public string GetString()
        {
            // Si l'on détecte un cas de mesure de type chaîne de caractères (ou autre castable en chaîne de caractères) on retour la valeur
            if (!String.IsNullOrEmpty(this._CurrentA64StrValue))
            {
                return this._CurrentA64StrValue;
            }
            // Sinon return null
            return null;
        }

        /// <summary>
        /// Called by Rainmeter when a measure is about to be destroyed. Perform cleanup here.
        /// --
        /// Libération des ressources utilisées
        /// </summary>
        public void Finalize()
        {
            this._A64Manager.CloseView();
        }
    }
}
