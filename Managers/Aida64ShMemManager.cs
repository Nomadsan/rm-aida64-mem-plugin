﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RMAida64MemPlugin
{
    public class Aida64ShMemManager
    {
        #region Win32 Imports
        public const int FILE_MAP_READ = 0x0004;

        /// <summary>
        /// Kernel Function : OpenFileMapping
        /// --
        /// Fonctionnalité Kernel : OpenFileMapping
        /// </summary>
        /// <param name="dwDesiredAccess">The dw desired access.</param>
        /// <param name="bInheritHandle">if set to <c>true</c> [b inherit handle].</param>
        /// <param name="lpName">Name of the lp.</param>
        /// <returns></returns>
        [DllImport("Kernel32", CharSet = CharSet.Auto, SetLastError = true)]
        internal static extern IntPtr OpenFileMapping(int dwDesiredAccess,
            bool bInheritHandle, StringBuilder lpName);

        /// <summary>
        /// Kernel Function : MapViewOfFile
        /// --
        /// Fonctionnalité Kernel : MapViewOfFile
        /// </summary>
        /// <param name="hFileMapping">The h file mapping.</param>
        /// <param name="dwDesiredAccess">The dw desired access.</param>
        /// <param name="dwFileOffsetHigh">The dw file offset high.</param>
        /// <param name="dwFileOffsetLow">The dw file offset low.</param>
        /// <param name="dwNumberOfBytesToMap">The dw number of bytes to map.</param>
        /// <returns></returns>
        [DllImport("Kernel32", CharSet = CharSet.Auto, SetLastError = true)]
        internal static extern IntPtr MapViewOfFile(IntPtr hFileMapping,
            int dwDesiredAccess, int dwFileOffsetHigh, int dwFileOffsetLow,
            int dwNumberOfBytesToMap);

        /// <summary>
        /// Kernel Function : UnmapViewOfFile
        /// --
        /// Fonctionnalité Kernel : UnmapViewOfFile
        /// </summary>
        /// <param name="map">The map.</param>
        /// <returns></returns>
        [DllImport("Kernel32.dll")]
        internal static extern bool UnmapViewOfFile(IntPtr map);

        /// <summary>
        /// Kernel Function : CloseHandle
        /// --
        /// Fonctionnalité Kernel : CloseHandle
        /// </summary>
        /// <param name="hObject">The h object.</param>
        /// <returns></returns>
        [DllImport("kernel32.dll")]
        internal static extern bool CloseHandle(IntPtr hObject);
        #endregion

        #region Properties
        #endregion

        #region Class Members
        private Boolean         _FileOpen       = false;
        private IntPtr          _MapOfFile      = IntPtr.Zero;
        private IntPtr          _FileHandle     = IntPtr.Zero;

        private const String    SHARED_MEM_NAME = "AIDA64_SensorValues";
        #endregion

        public Aida64ShMemManager() { }

        ~Aida64ShMemManager()
        {
            CloseView();
        }


        public bool OpenView()
        {
            if (!_FileOpen)
            {
                StringBuilder SharedMemFile = new StringBuilder(SHARED_MEM_NAME);
                _FileHandle = OpenFileMapping(FILE_MAP_READ, false, SharedMemFile);
                if (_FileHandle == IntPtr.Zero)
                {
                    throw new Exception("Unable to open file mapping.");
                }
                _MapOfFile = MapViewOfFile(_FileHandle, FILE_MAP_READ, 0, 0, 0);
                if (_MapOfFile == IntPtr.Zero)
                {
                    throw new Exception("Unable to read shared memory.");
                }
                _FileOpen = true;
            }
            return _FileOpen;
        }

        public void CloseView()
        {
            if (_FileOpen)
            {
                UnmapViewOfFile(_MapOfFile);
                CloseHandle(_FileHandle);
            }
        }

        public SharedMemXml GetData()
        {
            SharedMemXml XmlObj = null;

            if (_FileOpen)
            {
                String XmlSharedMemory = (String)Marshal.PtrToStringAnsi(_MapOfFile);

                if (!String.IsNullOrEmpty(XmlSharedMemory))
                {
                    XmlSharedMemory = $"<sharedmemxml>{XmlSharedMemory}</sharedmemxml>";

                    XmlSerializer XmlSer = new XmlSerializer(typeof(SharedMemXml));

                    using (TextReader reader = new StringReader(XmlSharedMemory))
                    {
                        XmlObj = (SharedMemXml)XmlSer.Deserialize(reader);
                    }
                }
                return XmlObj;
            }
            else
            {
                return null;
            }
        }

    }
}