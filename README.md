# Rainmeter Aida64 Shared Memory Plugin

This a project to provide Rainmeter with datas from computer's probes coming from Aida64 Software through Shared Memory
--
Projet permettant d'alimenter Rainmeter avec les données des sondes de l'ordinateur provenant d'Aida64 via la mémoire partagée