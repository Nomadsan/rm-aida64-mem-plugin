﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

// Rainmeter API
using Rainmeter;

namespace RMAida64MemPlugin
{
    public static class Plugin
    {
        static IntPtr StringBuffer = IntPtr.Zero;

        [DllExport]
        public static void Initialize(ref IntPtr Data, IntPtr RainMeterPtr)
        {
            Data = GCHandle.ToIntPtr(GCHandle.Alloc(new RMAida64MemPlugin()));
        }

        [DllExport]
        public static void Finalize(IntPtr DataPtr)
        {
            RMAida64MemPlugin RMAida64MemPlugin = (RMAida64MemPlugin)GCHandle.FromIntPtr(DataPtr).Target;

            RMAida64MemPlugin.Finalize();

            GCHandle.FromIntPtr(DataPtr).Free();

            if (StringBuffer != IntPtr.Zero)
            {
                Marshal.FreeHGlobal(StringBuffer);
                StringBuffer = IntPtr.Zero;
            }
        }

        [DllExport]
        public static void Reload(IntPtr Data, IntPtr RainMeterPtr, ref Double MaxValue)
        {
            RMAida64MemPlugin RMAida64MemPlugin = (RMAida64MemPlugin)GCHandle.FromIntPtr(Data).Target;
            RMAida64MemPlugin.Reload(new Rainmeter.API(RainMeterPtr), ref MaxValue);
        }

        [DllExport]
        public static double Update(IntPtr Data)
        {
            RMAida64MemPlugin RMAida64MemPlugin = (RMAida64MemPlugin)GCHandle.FromIntPtr(Data).Target;
            return RMAida64MemPlugin.Update();
        }

        [DllExport]
        public static IntPtr GetString(IntPtr Data)
        {
            RMAida64MemPlugin RMAida64MemPlugin = (RMAida64MemPlugin)GCHandle.FromIntPtr(Data).Target;
            if (StringBuffer != IntPtr.Zero)
            {
                Marshal.FreeHGlobal(StringBuffer);
                StringBuffer = IntPtr.Zero;
            }

            String StringValue = RMAida64MemPlugin.GetString();
            if (StringValue != null)
            {
                StringBuffer = Marshal.StringToHGlobalUni(StringValue);
            }

            return StringBuffer;
        }
    }
}
