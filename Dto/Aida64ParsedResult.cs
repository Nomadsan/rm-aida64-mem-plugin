﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RMAida64MemPlugin
{
    /*

    sys
    __id : String
    __label : String
    __value : Object
    pwr
    __id : String
    __label : String
    __value : Object
    volt
    __id : String
    __label : String
    __value : Object
    temp
    __id : String
    __label : String
    __value : Object


     */

    public class Meter
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "label")]
        public string Label { get; set; }
        [XmlElement(ElementName = "value")]
        public string Value { get; set; }
    }

    [XmlRoot(ElementName = "sharedmemxml")]
    public class SharedMemXml
    {
        [XmlElement(ElementName = "sys")]
        public List<Meter> Sys { get; set; }
        [XmlElement(ElementName = "temp")]
        public List<Meter> Temp { get; set; }
        [XmlElement(ElementName = "volt")]
        public List<Meter> Volt { get; set; }
        [XmlElement(ElementName = "pwr")]
        public List<Meter> Pwr { get; set; }
    }
}
