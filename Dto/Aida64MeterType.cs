﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMAida64MemPlugin
{
    public enum Aida64MeterType : byte
    {
        Undefined = 0,
        Pwr = 1,
        Sys = 2,
        Temp = 3,
        Volt = 4
    }
}
